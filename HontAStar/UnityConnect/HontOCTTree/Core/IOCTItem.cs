﻿using UnityEngine;
using System.Collections;

namespace Hont
{
    public interface IOCTItem<T>
    {
        Vector3 Position { get; }
        T Value { get; }
    }
}
