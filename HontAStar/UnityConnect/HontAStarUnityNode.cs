﻿using UnityEngine;
using System;
using System.Collections;

namespace Hont.AStar
{
    public class HontAStarUnityNode
    {
        HontAStarUnity mHost;
        Vector3 mMappingPos;
        Vector3 mCustomMappingPos;

        public Vector3 MappingPos { get { return mMappingPos; } }
        public Vector3 CustomMappingPos { get { return mCustomMappingPos; } }
        public Bounds Bounds { get { return new Bounds(MappingPos, mHost.MappingSize); } }


        public HontAStarUnityNode(HontAStarUnity host, HontAStarUnityPersistData.Node persistData)
        {
            this.mHost = host;
            this.mMappingPos = persistData.MappingPos;
            this.mCustomMappingPos = mMappingPos + persistData.CustomMappingLocalPos;
        }
    }
}
